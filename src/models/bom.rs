pub mod component;

use serde::{Deserialize, Serialize};

use crate::models::bom::component::Component;

#[allow(non_snake_case)]
#[derive(Deserialize, Serialize, Debug)]
pub struct Bom {
    pub bomFormat: String,
    pub specVersion: String,
    pub components: Vec<Component>,
}
