use serde::{Deserialize, Serialize};

#[allow(non_snake_case)]
#[derive(Deserialize, Serialize)]
pub struct Project {
    pub name: String,
    pub version: String,
    pub active: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub classifier: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub uuid: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lastBomImport: Option<usize>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lastBomImportFormat: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lastInheritedRiskScore: Option<f64>,
}

impl Project {
    pub fn new(name: String, version: String, active: bool) -> Self {
        Project {
            name,
            version,
            active,
            classifier: None,
            uuid: None,
            lastBomImport: None,
            lastBomImportFormat: None,
            lastInheritedRiskScore: None,
        }
    }
}
