use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug)]
pub struct Policy {
    pub version: String,
    pub vulnerability_rules: Option<Vec<Rule>>,
    pub licensing: Option<Licensing>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Licensing {
    pub permitted: Option<Vec<String>>,
    pub ignore: Option<Vec<IgnoredComponent>>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Rule {
    pub severity: String,
    pub max: usize,
    pub fail: bool,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct IgnoredComponent {
    pub name: String,
    pub version: String,
}
