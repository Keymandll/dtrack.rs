extern crate argparse;

use std::env::{self};
use std::fs::read_to_string;
use std::path::Path;

use argparse::{ArgumentParser, Store};
use url::Url;

use crate::models::policy::Policy;
use crate::models::project::Project;
use crate::remote::fetch_file;

// ----------------------------------------------------------------------------
// IMPL
// ----------------------------------------------------------------------------

pub struct ConfigError(pub &'static str);

pub struct Options {
    pub sbom_file: String,
}

pub struct Config {
    pub dtrack_url: String,
    pub dtrack_key: String,
    pub options: Options,
    pub project: Project,
    pub policy: Option<Policy>,
}

pub fn get_dtrack_url() -> Result<String, ConfigError> {
    let value = match env::var("DTRACK_SERVER") {
        Ok(value) => { value },
        Err(_) => return Err(ConfigError("The Dependency Track URL must be provided using the DTRACK_SERVER environment variable."))
    };
    if Url::parse(&value).is_err() {
        return Err(ConfigError(
            "The Dependency Track URL set via the DTRACK_SERVER environment variable is invalid.",
        ));
    }
    if !value.starts_with("https://") {
        return Err(ConfigError(
            "The Dependency Track URL set via the DTRACK_SERVER must use the HTTPS protocol.",
        ));
    }
    Ok(value)
}

pub fn get_dtrack_key() -> Result<String, ConfigError> {
    let value = match env::var("DTRACK_API_KEY") {
        Ok(value) => { value },
        Err(_) => return Err(ConfigError("The Dependency Track API key must be provided using the DTRACK_API_KEY environment variable."))
    };
    if value.len() < 32 {
        return Err(ConfigError("The Dependency Track API key set via the DTRACK_API_KEY environment variable is invalid."));
    }
    Ok(value)
}

fn validate_project_name(value: &String) -> bool {
    value.len() > 1
}

fn validate_project_version(value: &String) -> bool {
    value.len() > 1
}

fn validate_sbom_file(value: String, default: &str) -> String {
    if Path::new(&value).exists() {
        return value;
    }
    String::from(default)
}

fn get_options() -> Result<(Project, Options), ConfigError> {
    let mut project_name = String::new();
    let mut project_version = String::new();
    let mut sbom_file = String::new();

    {
        let mut ap = ArgumentParser::new();
        ap.set_description("Manage dependency vulnerabilities and license compliance in the CI using Dependency Track. Make sure you set the `DTRACK_URL` and `DTRACK_API_KEY` environment variables.");
        ap.refer(&mut project_name)
            .add_option(&["-p", "--project"], Store, "Project name")
            .required();
        ap.refer(&mut project_version)
            .add_option(&["-v", "--version"], Store, "Project version")
            .required();
        ap.refer(&mut sbom_file).add_option(
            &["-s", "--sbom"],
            Store,
            "Path to SBOM file (default: 'bom.json')",
        );
        ap.parse_args_or_exit();
    }

    if !validate_project_name(&project_name) {
        return Err(ConfigError("Invalid project name"));
    }
    if !validate_project_version(&project_version) {
        return Err(ConfigError("Invalid project version"));
    }
    let sbom_file = validate_sbom_file(sbom_file, "bom.json");
    let project = Project::new(project_name, project_version, true);
    let options = Options { sbom_file };

    Ok((project, options))
}

fn load_policy(location: Option<String>) -> Result<Option<Policy>, ConfigError> {
    if location.is_none() {
        return Ok(None);
    }
    let location = location.unwrap();

    let policy_data = if location.starts_with("https://") {
        // Load remote policy
        match fetch_file(&location) {
            Ok(data) => data,
            Err(_) => return Err(ConfigError("Failed to load remote policy file.")),
        }
    } else {
        // Load local policy
        match read_to_string(location) {
            Ok(data) => data,
            Err(_) => return Err(ConfigError("Failed to load local policy file.")),
        }
    };
    let policy: Policy = match serde_yaml::from_str(&policy_data) {
        Ok(data) => data,
        Err(_) => return Err(ConfigError("Invalid Policy document.")),
    };
    Ok(Some(policy))
}

pub fn get_policy() -> Result<Option<Policy>, ConfigError> {
    let mut value: Option<String> = match env::var("DTRACK_POLICY") {
        Ok(value) => Some(value),
        Err(_) => None,
    };
    if value.is_none() && Path::new(".dtrack-policy.yml").exists() {
        value = Some(String::from(".dtrack-policy.yml"));
    }
    load_policy(value)
}

pub fn get() -> Result<Config, ConfigError> {
    let result = match get_options() {
        Ok(result) => result,
        Err(error) => return Err(error),
    };

    let dtrack_url = match get_dtrack_url() {
        Ok(value) => value,
        Err(error) => return Err(error),
    };

    let dtrack_key = match get_dtrack_key() {
        Ok(value) => value,
        Err(error) => return Err(error),
    };

    let policy = match get_policy() {
        Ok(data) => data,
        Err(e) => return Err(e),
    };

    Ok(Config {
        dtrack_url,
        dtrack_key,
        project: result.0,
        options: result.1,
        policy,
    })
}

// ----------------------------------------------------------------------------
// TESTS
// ----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use serial_test::serial;
    use std::env::{remove_var, set_var};

    #[test]
    #[serial]
    fn get_dtrack_url() {
        let expected = String::from("https://localhost:8000");
        set_var("DTRACK_SERVER", "https://localhost:8000");
        let result = super::get_dtrack_url();
        assert_eq!(result.is_ok(), true);
        assert_eq!(result.ok().unwrap(), expected);
    }

    #[test]
    #[serial]
    fn get_dtrack_url_unset() {
        remove_var("DTRACK_SERVER");
        let result = super::get_dtrack_url();
        assert_eq!(result.is_err(), true);
    }

    #[test]
    #[serial]
    fn get_dtrack_url_invalid() {
        set_var("DTRACK_SERVER", "invalid");
        let result = super::get_dtrack_url();
        assert_eq!(result.is_err(), true);
    }

    #[test]
    #[serial]
    fn get_dtrack_key() {
        let expected = String::from("AAAAAAAAAABBBBBBBBBBCCCCCCCCCCDD");
        set_var("DTRACK_API_KEY", &expected);
        let result = super::get_dtrack_key();
        assert_eq!(result.is_ok(), true);
        assert_eq!(result.ok().unwrap(), expected);
    }

    #[test]
    #[serial]
    fn get_dtrack_key_unset() {
        remove_var("DTRACK_API_KEY");
        let result = super::get_dtrack_key();
        assert_eq!(result.is_err(), true);
    }

    #[test]
    #[serial]
    fn get_dtrack_key_invalid() {
        set_var("DTRACK_API_KEY", "AAAAAAAA");
        let result = super::get_dtrack_key();
        assert_eq!(result.is_err(), true);
    }

    #[test]
    fn validate_project_name() {
        let value = String::from("test");
        assert_eq!(super::validate_project_name(&value), true);
        let value = String::from("");
        assert_eq!(super::validate_project_name(&value), false);
    }

    #[test]
    fn validate_project_version() {
        let value = String::from("test");
        assert_eq!(super::validate_project_version(&value), true);
        let value = String::from("");
        assert_eq!(super::validate_project_version(&value), false);
    }

    #[test]
    fn validate_sbom_file() {
        let expected = String::from(std::env::current_dir().unwrap().to_str().unwrap());
        let value = super::validate_sbom_file(expected.clone(), "bom.json");
        assert_eq!(value, expected);

        let value = super::validate_sbom_file(String::from("/tmp/unittest-deadbeef"), "bom.json");
        assert_eq!(value, "bom.json");
    }
}
