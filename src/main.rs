mod analysis;
mod bom;
mod config;
mod license;
mod models;
mod remote;
mod urls;
mod utils;

use analysis::{should_fail_test, SeverityCount};
use config::Config;
use license::is_license_permitted;
use models::bom::Bom;
use remote::{ensure_project, fetch_vulnerabilities, upload_bom, wait_for_processing};

use models::dependencytrack::vulnerability::Vulnerabilities;

// ----------------------------------------------------------------------------
// IMPL
// ----------------------------------------------------------------------------

fn process_vulnerabilities(config: &Config, vulnerabilities: Vulnerabilities) -> bool {
    if config.policy.is_none() {
        return false;
    }
    let filtered = vulnerabilities.get_valid_issues();
    if filtered.0.is_empty() {
        return false;
    }
    let severity_count = SeverityCount::new(&filtered);

    utils::print_header("VULNERABLE DEPENDENCIES");
    println!(
        "{:<30} {:<18} {:<10} {:<30}",
        "NAME", "VERSION", "SEVERITY", "VULNERABILITY"
    );
    println!("{}", "-".repeat(80));

    for vulnerability in filtered.0.iter() {
        println!(
            "{:<30} {:<18} {:<10} {:<30}",
            vulnerability.component.name,
            vulnerability.component.version,
            vulnerability.vulnerability.severity,
            vulnerability.vulnerability.vulnId,
        );
    }

    severity_count.print();
    should_fail_test(config, &severity_count)
}

fn main() {
    let mut config = match config::get() {
        Ok(c) => c,
        Err(e) => {
            println!("[ERROR] {}", e.0);
            std::process::exit(255);
        }
    };

    let project = match ensure_project(&config) {
        Ok(project) => project,
        Err(e) => {
            println!("[ERROR] {}", e.0);
            std::process::exit(255);
        }
    };

    config.project = project;

    // Submit BOM and fetch BOM token
    let bom_data = bom::get(config.options.sbom_file.clone());

    let bom_data = match bom_data {
        Ok(data) => data,
        Err(e) => {
            println!("[ERROR] {}", e.0);
            std::process::exit(255);
        }
    };

    let b64_bom_data = base64::encode(&bom_data);
    let token = match upload_bom(&config, &b64_bom_data) {
        Ok(token) => token,
        Err(e) => {
            println!("[ERROR] {}", e.0);
            std::process::exit(255);
        }
    };

    // Wait until BOM is processed
    wait_for_processing(&config, token);

    // Process vulnerabilities
    let vulnerabilities = match fetch_vulnerabilities(&config) {
        Ok(violations) => violations,
        Err(e) => {
            println!("[ERROR] {}", e.0);
            std::process::exit(255);
        }
    };
    let fail_vulnerabilities = process_vulnerabilities(&config, vulnerabilities);

    // Check the policy for license violations
    // We do not rely on DependencyTrack for this information as it cannot
    // handle license *expressions*, thus in certain case it won't recognize
    // licenses.

    let bom_data: Bom = match serde_json::from_str(&bom_data) {
        Ok(data) => data,
        Err(e) => panic!("Failed to parse BOM file: {}", e),
    };
    let fail_license = !is_license_permitted(&config, &bom_data);

    if fail_license || fail_vulnerabilities {
        std::process::exit(1);
    }
}
