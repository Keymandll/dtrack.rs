use std::fs::read_to_string;

pub struct BomError(pub String);

pub fn get(bom_file: String) -> Result<String, BomError> {
    let bom_data = match read_to_string(bom_file) {
        Ok(data) => data,
        Err(e) => return Err(BomError(e.to_string())),
    };
    Ok(bom_data)
}
