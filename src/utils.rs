pub fn print_header(title: &str) {
    println!("\n{}\n{}\n{}", "=".repeat(80), title, "=".repeat(80));
}
