use core::time;
use reqwest::blocking::Response;
use reqwest::{Error, Method, StatusCode};
use serde_json::json;
use std::thread::sleep;

use crate::config::Config;

use crate::models::project::Project;

use crate::models::dependencytrack::vulnerability::Vulnerabilities;

use crate::models::dependencytrack::bom::{BomStatus, BomToken};
use crate::urls::{
    get_bom_token_url, get_bom_upload_url, get_create_project_url, get_lookup_project_url,
    get_vulnerabilities_url,
};

// ----------------------------------------------------------------------------
// IMPL
// ----------------------------------------------------------------------------

pub struct RemoteError(pub String);

pub fn wait_for_processing(config: &Config, token: BomToken) {
    let mut is_processing = true;
    let wait_time = time::Duration::from_millis(1000);
    while is_processing {
        is_processing = match is_bom_processing(config, &token) {
            Ok(processing) => processing,
            Err(e) => {
                println!("[ERROR] {}", e.0);
                std::process::exit(1);
            }
        };
        sleep(wait_time);
    }
}

fn get_client(config: &Config, method: Method, url: String) -> reqwest::blocking::RequestBuilder {
    let client = reqwest::blocking::Client::new();
    client
        .request(method, url)
        .header("X-API-Key", &config.dtrack_key)
}

pub fn fetch_file(location: &String) -> Result<String, RemoteError> {
    let client = reqwest::blocking::Client::new();
    let result = match client.get(location).send() {
        Ok(data) => data,
        Err(error) => return Err(RemoteError(error.to_string())),
    };

    if result.status() != StatusCode::OK {
        return Err(RemoteError(
            "Failed to load Dtrack policy file.".to_string(),
        ));
    }

    let data = match result.text() {
        Ok(data) => data,
        Err(error) => return Err(RemoteError(error.to_string())),
    };
    Ok(data)
}

pub fn fetch_vulnerabilities(config: &Config) -> Result<Vulnerabilities, RemoteError> {
    let url = get_vulnerabilities_url(config);
    let client = get_client(config, Method::GET, url);
    let result = match client.send() {
        Ok(response) => response,
        Err(error) => return Err(RemoteError(error.to_string())),
    };
    let result = match validate_status_code(result) {
        Ok(data) => data,
        Err(error) => return Err(error),
    };
    let vulnerabilities: Vulnerabilities = match result.json() {
        Ok(data) => data,
        Err(error) => return Err(get_unexpected_error(error)),
    };
    Ok(vulnerabilities)
}

pub fn upload_bom(config: &Config, data: &String) -> Result<BomToken, RemoteError> {
    let url = get_bom_upload_url(config);
    let client = get_client(config, Method::PUT, url);

    let json_data = json!({
        "project": &config.project.uuid,
        "bom": data,
    });

    let result = match client.json(&json_data).send() {
        Ok(response) => response,
        Err(error) => return Err(RemoteError(error.to_string())),
    };

    let result = match validate_status_code(result) {
        Ok(data) => data,
        Err(error) => return Err(error),
    };

    let token: BomToken = match result.json() {
        Ok(data) => data,
        Err(error) => return Err(get_unexpected_error(error)),
    };
    Ok(token)
}

pub fn ensure_project(config: &Config) -> Result<Project, RemoteError> {
    let url = get_lookup_project_url(config);
    let client = get_client(config, Method::GET, url);
    let result = match client.send() {
        Ok(response) => response,
        Err(error) => return Err(RemoteError(error.to_string())),
    };

    if result.status() == StatusCode::NOT_FOUND {
        return create_project(config);
    }

    let result = match validate_status_code(result) {
        Ok(data) => data,
        Err(error) => return Err(error),
    };

    let project: Project = match result.json() {
        Ok(data) => data,
        Err(error) => return Err(get_unexpected_error(error)),
    };
    Ok(project)
}

pub fn create_project(config: &Config) -> Result<Project, RemoteError> {
    let url = get_create_project_url(config);
    let client = get_client(config, Method::PUT, url);
    let result = match client.json(&config.project).send() {
        Ok(response) => response,
        Err(error) => return Err(RemoteError(error.to_string())),
    };
    let result = match validate_status_code(result) {
        Ok(data) => data,
        Err(error) => return Err(error),
    };
    let project: Project = match result.json() {
        Ok(data) => data,
        Err(error) => return Err(get_unexpected_error(error)),
    };
    Ok(project)
}

fn is_bom_processing(config: &Config, token: &BomToken) -> Result<bool, RemoteError> {
    let url = get_bom_token_url(config, token);
    let client = get_client(config, Method::GET, url);
    let result = match client.send() {
        Ok(response) => response,
        Err(error) => return Err(RemoteError(error.to_string())),
    };
    let result = match validate_status_code(result) {
        Ok(data) => data,
        Err(error) => return Err(error),
    };
    let processing: BomStatus = match result.json() {
        Ok(data) => data,
        Err(error) => return Err(get_unexpected_error(error)),
    };
    Ok(processing.processing)
}

fn validate_status_code(result: Response) -> Result<Response, RemoteError> {
    let result = match result.status() {
        StatusCode::OK => result,
        StatusCode::CREATED => result,
        StatusCode::UNAUTHORIZED => {
            return Err(RemoteError(
                "Failed to authenticate to Dependency Track. Please check your API key."
                    .to_string(),
            ))
        }
        StatusCode::CONFLICT => {
            return Err(RemoteError(
                "Failed to create project because it already exists.".to_string(),
            ))
        }
        _status_code => {
            return Err(RemoteError(format!(
                "Unexpected error ({}).",
                result.status()
            )))
        }
    };
    Ok(result)
}

fn get_unexpected_error(error: Error) -> RemoteError {
    let mut message = String::from("Unexpected response from Dependency Track: ");
    message.push_str(&error.to_string()[..]);
    RemoteError(message)
}

// ----------------------------------------------------------------------------
// TESTS
// ----------------------------------------------------------------------------
